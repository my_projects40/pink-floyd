import socket
import data
import hashlib
PASSWORD = '980308c78a411993c5618ef7aaf2abb6'  #Janetka is sassy

"""
This function returns the makeMainDic from the data.py
input: none
output: dect
"""
def get_the_dataBase():
    dect = data.makeMainDic()
    return dect


"""
This finction returns the data of response of client_msg
input: str he recieve it like a string
output: data
"""
def get_theDate(str):
    str = str.split(":")
    data = str[2]
    return data

"""
This function returns the fiction(the task she has to do) of client message
input:client_msg
output: send_to_fiction
"""
def fun(client_msg):
    if "1" in client_msg:
        data_base = get_the_dataBase()
        send_to_fiction = data.albums(data_base)



    data_base = get_the_dataBase()

    if "2" in client_msg:
        base = get_theDate(client_msg)
        send_to_fiction = data.songs_by_album(data_base, base)


    if "3" in client_msg:
        base = get_theDate(client_msg)
        send_to_fiction = data.song_length_Byname(data_base, base)

    if "4" in client_msg:
        base = get_theDate(client_msg)
        send_to_fiction = data.lyrichs_of_song(data_base, base)

    if "5" in client_msg:
        base = get_theDate(client_msg)
        send_to_fiction = data.fromSong_to_Album(data_base, base)

    if "6" in client_msg:
        base = get_theDate(client_msg)
        send_to_fiction = data.findingSong_byName(data_base, base)

    if "7" in client_msg:
        base = get_theDate(client_msg)
        send_to_fiction = data.findingSong_byLyrichs(data_base, base)

    if client_msg == "400:Quit":
        return 2
    return send_to_fiction

def main():

    with socket.socket() as listening_sock:

        listening_sock.bind(('',9090))
        listening_sock.listen(1)

        while True:
            client_soc, client_address = listening_sock.accept()
            with client_soc:

                client_soc.sendall(b"WELCOME")
                client_msg = ' '
                login = ' '
                while login != "yes":
                    passw = client_soc.recv(1024)
                    passw = passw.decode()
                    passw = hashlib.md5(passw.encode('utf-8')).hexdigest()
                    if passw == PASSWORD:
                        key = "yes"
                        client_soc.sendall(key.encode())
                    else:
                        key = "no"
                        client_soc.sendall(key.encode())

                while "8" not in client_msg:
                    try:
                        client_msg = client_soc.recv(1024)
                        client_msg = client_msg.decode()
                    except Exception as ex:
                        print("Exception:" + ex)
                        break

                    if '8' not in client_msg:
                        res = fun(client_msg)

                    if res == 2:
                        client_soc.close()
                    else:
                        client_soc.sendall(res.encode())


if __name__ == "__main__":
    main()
