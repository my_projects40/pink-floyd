"""
This function makes the dictionary in dictionary that contains list with length and lyrichs and returns the answer(main_dec), this function contains the function reemovNestings
makeMainDic
input: none
output: main_dec
reemovNestings
input: l, output
output: none
"""
def makeMainDic():
    def reemovNestings(l, output):

        for i in l:
            if type(i) == list:
                reemovNestings(i, output)
            else:
                output.append(i)

    with open("Pink_Floyd_DB.txt", 'r') as file_content:
        str = file_content.read()

        output = []

        albums = str.split("#")

        main_dec = {} # key: album_name+, value: song_dec(1-8)
        temp = {}

        song_dec = {}  #key: song name, value: [length, lyrics]
        albums = albums[1:]



        for i in albums:
            song_list = i.split('*')


            len  = 0
            for j in song_list:


                if len != 0:


                    lyrichs = j.split("::")
                    song_name = lyrichs[:1]
                    song_name = ''.join(song_name)

                    lyrichs_time = [lyrichs[2:-1], [lyrichs[3:]]]
                    reemovNestings(lyrichs_time, output)
                    temp[song_name] = output

                    output = []

                else:
                    album_name_full = j
                    album_name = album_name_full.split("::")
                    del album_name[-1]
                    album_name = ''.join(album_name) #getting just the album name not the year

                    len = 1;


            main_dec[album_name] = temp

            temp = {}
        return  main_dec


"""
This function returns the albums in the givven file
input: dect - that we recieved from the makeMainDic function 
output: return_data - there is the names of albums
"""
def albums(dect):

    return_data = []
    for key in dect:
        return_data.append(key)
    return_data = ', '.join(return_data)

    return return_data

"""
This function gives songs by a name of an album that user choose
input:dect - that we recieved from the makeMainDic function , album_name - users's choice of an album
output:return_songs - it returns all the songs 
"""
def songs_by_album(dect, album_name):
    return_songs = []
    for p_id, p_info in dect.items():
        if album_name == p_id:
            for key in p_info:
                return_songs.append(key)
            return_songs = ', '.join(return_songs)
            return return_songs

"""
This function returns the length of the song that user gets
input: dect, name_of_song
output: i[0] - lenght of the song
"""
def song_length_Byname(dect, name_of_song):
    for p_id, p_info in dect.items():
        for key in p_info:
            i = p_info[key]
            if name_of_song == key:
                return i[0]
"""
This function returns the lyrichs of song that user gets
input: dect, name_of_song
output: i[1] - lyrichs
"""
def lyrichs_of_song(dect, name_of_song):
    for p_id, p_info in dect.items():
        for key in p_info:
            i = p_info[key]
            if name_of_song == key:
                return i[1]

"""
This function gets tha name of the song and returns the name of an album
input:dect, name_of_song
output:p_id - the album
"""
def fromSong_to_Album(dect, name_of_song):
    for p_id, p_info in dect.items():
        for key in p_info:
            i = p_info[key]
            if name_of_song == key:
                return p_id

"""
This function find a song by user's inputs 
input: dect, check_word - user gives the word, the name of a song
output: return_songs - options of songs that consist of the word he gave
"""
def findingSong_byName(dect, check_word):
    return_songs = []
    for p_info in dect.values():
        i = p_info.keys()
        for key in i:
            if check_word.upper() in key.upper():
                return_songs.append(key)
    return_songs = ', '.join(return_songs)
    return return_songs
"""
This function find Song by Lyrichs
input: dect, check_word
output: list_of_names -  returns the names of songs that contain the word
"""
def findingSong_byLyrichs(dect, check_word):
    list_of_names = []
    for p_info in dect.values():
        j = p_info.keys()
        for keys in j:
            i = p_info[keys]
            if check_word.lower() in i[1].lower():
                list_of_names.append(keys)

    list_of_names = ', '.join(list_of_names)
    return list_of_names



